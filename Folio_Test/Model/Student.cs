﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Folio_Test.Model
{

    public class Student
    {
        public int ID { get; set; }
        [Required]
        [StringLength(50 ,ErrorMessage = "Surname name cannot be longer than 50 characters.")]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "First name cannot be longer than 50 characters.")]
        [Display(Name = "First Name")]
        public string FirstMidName { get; set; }

        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        [DataType(DataType.Date)]
        public DateTime EnrollmentDate { get; set; }

        [Required]
        [Display(Name = "GPA")]
        public double Gpa { get; set; }

        [Required]
        [Range(typeof(int), "0", "99", ErrorMessage = "{0} must be a decimal/number between {1} and {2}.")]
        public int Age { get; set; }

        public string StudentName
        {
            get
            {
                return LastName + ", " + FirstMidName;
            }
        }
        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }

}