﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Folio_Test.Model
{

    public class Course
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CourseID { get; set; }

        [Required]
        [Display(Name = "Class Name")]
        public string CourseTitle { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Location cannot be longer than 75 characters.")]
        public string Location { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "Name cannot be longer than 50 characters.")]
        public string TeacherName { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}
