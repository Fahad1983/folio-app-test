namespace Folio_Test.Migrations
{
    using Folio_Test.Model;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Folio_Test.DAL.FolioContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(Folio_Test.DAL.FolioContext context)
        {
            var students = new List<Student>
            {
            new Student{FirstMidName="Jhon",LastName="Packer",EnrollmentDate=DateTime.Parse("2017-09-01"), Gpa=2.8,Age=19},
            new Student{FirstMidName="Peter",LastName="Jhonston",EnrollmentDate=DateTime.Parse("2018-09-01"), Gpa=2.8,Age=19},
            new Student{FirstMidName="Robert",LastName="Smith",EnrollmentDate=DateTime.Parse("2013-09-01"), Gpa=3.2,Age=19},
            new Student{FirstMidName="Louis",LastName="Thomson",EnrollmentDate=DateTime.Parse("2012-09-01"), Gpa=2.8,Age=19},
            new Student{FirstMidName="Yan",LastName="Li",EnrollmentDate=DateTime.Parse("2012-09-01"), Gpa=2.8,Age=19},
            new Student{FirstMidName="Peggy",LastName="Justice",EnrollmentDate=DateTime.Parse("2011-09-01"), Gpa=2.8,Age=19},
            new Student{FirstMidName="Laura",LastName="Norman",EnrollmentDate=DateTime.Parse("2018-09-01"), Gpa=3.0,Age=19},
            new Student{FirstMidName="Nino",LastName="Olivetto",EnrollmentDate=DateTime.Parse("2017-09-01"), Gpa=3.2,Age=19}
            };

            students.ForEach(s => context.Students.Add(s));
            context.SaveChanges();
            var courses = new List<Course>
            {
                new Course{CourseID=1050, CourseTitle="UI/UX",Location="Building 5 Room 501" ,TeacherName="Mr Jhonston" },
                new Course{CourseID=4022,CourseTitle="DevOps",Location="Building 2 Room 606" ,TeacherName="Miss Thomson" }
            };
            courses.ForEach(s => context.Courses.Add(s));
            context.SaveChanges();
            var enrollments = new List<Enrollment>
            {
                new Enrollment{StudentID=1,CourseID=1050 },
                new Enrollment{StudentID=2,CourseID=1050 },
                new Enrollment{StudentID=3,CourseID=1050 },
                new Enrollment{StudentID=4,CourseID=1050 },
                new Enrollment{StudentID=5,CourseID=4022 },
                new Enrollment{StudentID=6,CourseID=4022},
                new Enrollment{StudentID=7,CourseID=4022},
                new Enrollment{StudentID=8,CourseID=4022 }


            };
            enrollments.ForEach(s => context.Enrollments.Add(s));
            context.SaveChanges();

        }
    }
}
