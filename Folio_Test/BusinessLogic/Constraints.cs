﻿using Folio_Test.DAL;
using Folio_Test.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Folio_Test.BusinessLogic
{
    public static class Constraints
    {
        public static  bool SurName_Uniqueness(Student student)
        {
              FolioContext db=new FolioContext();
             bool flag = false;
                var studentsExists = db.Students.AsEnumerable().Any(i=> i.LastName.ToLower() == student.LastName.ToLower());
            if(studentsExists)
            {
                flag = true;
            }
            return flag;
        }
    }
}