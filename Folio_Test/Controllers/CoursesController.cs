﻿using Folio_Test.DAL;
using Folio_Test.Model;
using Folio_Test.Repo;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Folio_Test.Controllers
{
    public class CoursesController : Controller
    {
       
        private IFolioDb db;

        public CoursesController()
        {
            db = new FolioContext();
        }

        public CoursesController(IFolioDb _db)
        {
            db = _db;
        }
            // GET: Courses
            public ActionResult Index()
        {
             return View(db.Query<Course>().ToList());
        }

        // GET: Courses/Details/5
        public ActionResult Details(int? id)
        {
            List<Course> listCourses = new List<Course>();
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
           Course  selectedcourse = db.Query<Course>().Single(r => r.CourseID == id);
            listCourses.Add(selectedcourse);
            Session["CourseTitle"] = selectedcourse.CourseTitle;
            Session["CourseID"] = selectedcourse.CourseID;
            Session["Course"] = listCourses;
            if (selectedcourse == null)
            {
                return HttpNotFound();
            }


            if (Request.IsAjaxRequest())
            {
         
                return PartialView("_studentPartialView",Session["Course"] as List<Course> );
            }
            else
            { return View(selectedcourse); }


        }
      
        // GET: Courses/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Courses/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "CourseID,CourseTitle,Location,TeacherName")] Course course)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Add(course);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            return View(course);
        }

        // GET: Courses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course = db.Query<Course>().Single(r => r.CourseID == id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        // POST: Courses/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "CourseID,CourseTitle,Location,TeacherName")] Course course)
        {
            if (ModelState.IsValid)
            {
                db.Update(course);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(course);
        }

        // GET: Courses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Course course =  db.Query<Course>().Single(r => r.CourseID == id);
            if (course == null)
            {
                return HttpNotFound();
            }
            return View(course);
        }

        // POST: Courses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Course course = db.Query<Course>().Single(r => r.CourseID == id);
            db.Remove(course);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
