﻿using Folio_Test.BusinessLogic;
using Folio_Test.DAL;
using Folio_Test.Model;
using Folio_Test.Repo;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web.Mvc;

namespace Folio_Test.Controllers
{
    public class StudentsController : Controller
    {
        private IFolioDb db ;

        public StudentsController()
        {
            db = new FolioContext();
        }

        public StudentsController(IFolioDb _db)
        {
            db = _db;
        }
  

        // GET: Students
        public ActionResult Index()
        {
            return View(db.Query<Student>().ToList());
        }

        // GET: Students/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Query<Student>().Single(r => r.ID == id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // GET: Students/Create
        public ActionResult Create()
        {
            if (Session.Keys.Count != 0)
            { ViewBag.Title = Session["CourseTitle"].ToString();}
            return View();
        }

        // POST: Students/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,LastName,FirstMidName,EnrollmentDate,Gpa,Age")] Student student)
        {
            int courseId = 0; bool enrollmentBit = false; var enrollment = new Enrollment();

            if (Session.Keys.Count != 0)
            {
                ViewBag.Title = Session["CourseTitle"].ToString();
                enrollmentBit = int.TryParse(Session["CourseID"].ToString(), out courseId);
            }
            try
            {
               if(Constraints.SurName_Uniqueness(student))
                {
                   ModelState.AddModelError("", "Student Already exists with same Surname");
                }
                if (ModelState.IsValid)
                {
                    db.Add(student);
                    db.SaveChanges();

                    if (enrollmentBit)
                    {
                        enrollment = new Enrollment { CourseID = courseId, StudentID = student.ID };
                        db.Add(enrollment);
                        db.SaveChanges();
                    }
                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            return View(student);
        }

        // GET: Students/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Query<Student>().Single(r => r.ID == id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Students/Edit/5
 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,LastName,FirstMidName,EnrollmentDate,Gpa,Age")] Student student)
        {
            try
            {
           
            if (ModelState.IsValid)
            {
                db.Update(student);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }

            return View(student);
        }

        // GET: Students/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Query<Student>().Single(r => r.ID == id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Students/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Student student = db.Query<Student>().Single(r => r.ID == id);
            db.Remove(student);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
