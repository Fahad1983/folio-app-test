﻿using Folio_Test.Model;
using Folio_Test.Repo;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Collections.Generic;

namespace Folio_Test.DAL
{
    public class FolioContext : DbContext, IFolioDb
    {

        public FolioContext() : base("FolioContext")
        {

        }

        public DbSet<Student> Students { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet<Course> Courses { get; set; }

        IQueryable<T> IFolioDb.Query<T>()
        {
            return Set<T>();
        }

        void IFolioDb.Add<T>(T entity)
        {
            Set<T>().Add(entity);
        }

        void IFolioDb.Update<T>(T entity)
        {
            Entry(entity).State = EntityState.Modified;
        }

        void IFolioDb.Remove<T>(T entity)
        {
            Set<T>().Remove(entity);
        }

        void IFolioDb.SaveChanges()
        {
            SaveChanges();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}