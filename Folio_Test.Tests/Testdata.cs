﻿using Folio_Test.Model;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Folio_Test.Tests
{
  public  class Testdata
    {
        public static IQueryable<Student> Students
        {
            get
            {
                var students = new List<Student>()
            {
            new Student{FirstMidName="Jhon",LastName="Packer",EnrollmentDate=DateTime.Parse("2017-09-01"), Gpa=2.8,Age=19},
            new Student{FirstMidName="Peter",LastName="Jhonston",EnrollmentDate=DateTime.Parse("2018-09-01"), Gpa=2.8,Age=19},
            new Student{FirstMidName="Robert",LastName="Smith",EnrollmentDate=DateTime.Parse("2013-09-01"), Gpa=3.2,Age=19},
            new Student{FirstMidName="Louis",LastName="Thomson",EnrollmentDate=DateTime.Parse("2012-09-01"), Gpa=2.8,Age=19},
            new Student{FirstMidName="Yan",LastName="Li",EnrollmentDate=DateTime.Parse("2012-09-01"), Gpa=2.8,Age=19},
            new Student{FirstMidName="Peggy",LastName="Justice",EnrollmentDate=DateTime.Parse("2011-09-01"), Gpa=2.8,Age=19},
            new Student{FirstMidName="Laura",LastName="Norman",EnrollmentDate=DateTime.Parse("2018-09-01"), Gpa=3.0,Age=19},
            new Student{FirstMidName="Nino",LastName="Olivetto",EnrollmentDate=DateTime.Parse("2017-09-01"), Gpa=3.2,Age=19}
            };
                return students.AsQueryable();
            }
        }

    }
}
