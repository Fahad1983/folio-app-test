﻿using Folio_Test.Controllers;
using Folio_Test.Model;
using Folio_Test.Tests.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Globalization;
using System.Linq;


namespace Folio_Test.Tests.Controllers
{
    [TestClass]
    public class StudentControllerTest
    {
        [TestMethod]
        public void TestCreationOfStudentWithUniqueSurnameConstraint()
        {
            //Arange
            var db = new FakeFolioDb();
            db.AddSet(Testdata.Students);

            //Act
            bool flag = false;
            StudentsController controller = new StudentsController(db);
            Student student = new Student { Age = 22, EnrollmentDate = DateTime.ParseExact("2017-09-01", "yyyy-mm-dd", CultureInfo.InvariantCulture), LastName = "Olivetto", FirstMidName = "Parker", Gpa = 2.4 };
            if (SurName_Uniqueness(student, db))
            {
                flag = true;
            }

            //Assert
            Assert.IsTrue(flag, "Student with the surname already added");
        }


        private static bool SurName_Uniqueness(Student student, FakeFolioDb db)
        {
            bool flag = false;
            var studentsExists = db.Query<Student>().Any(i => i.LastName.ToLower() == student.LastName.ToLower());
            if (studentsExists)
            {
                flag = true;
            }
            return flag;
        }
    }
}
